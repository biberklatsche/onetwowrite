library i18n;

import '../utils/utils.dart' as Utils;

final String DEFAULT_DOCUMENT_NAME = '{{defaultDocumentName}}';

final String NEW_BUTTON_CAPTION = '{{newButtonCaption}}';
final String SAVE_BUTTON_CAPTION = '{{saveButtonCaption}}';
final String SAVE_AS_BUTTON_CAPTION = '{{saveAsButtonCaption}}';
final String COPY_BUTTON_CAPTION = '{{copyButtonCaption}}';
final String DELETE_BUTTON_CAPTION = '{{deleteButtonCaption}}';
final String DOCUMENTS_BUTTON_CAPTION = '{{documentsButtonCaption}}';
final String EXPORT_MD_BUTTON_CAPTION = '{{exportMdButtonCaption}}';
final String EXPORT_HTML_BUTTON_CAPTION = '{{exportHtmlButtonCaption}}';
final String EXPORT_PDF_BUTTON_CAPTION = '{{exportPdfButtonCaption}}';
final String IMPORT_BUTTON_CAPTION = '{{importButtonCaption}}';

final String SEARCH_INPUT_FIELD_PLACEHOLDER = '{{searchInputField}}';
final String REPLACE_INPUT_FIELD_PLACEHOLDER = '{{replaceInputField}}';
final String REPLACE_CHECKBOX = '{{replaceCheckBox}}';
final String REPLACE_BUTTON = '{{replaceButton}}';
final String REPLACE_ALL_BUTTON = '{{replaceAllButton}}';

final String SETTINGS_CAPTION = '{{settingsCaption}}';
final String SETTINGS_FONTS_CAPTION = '{{settingsFontsCaption}}';
final String SETTINGS_FONTS_DESCRIPTION = '{{settingsFontsDescription}}';
final String SETTINGS_FONTS_AUTO = '{{settingsFontsAuto}}';
final String SETTINGS_FONTS_SMALL = '{{settingsFontsSmall}}';
final String SETTINGS_FONTS_MEDIUM = '{{settingsFontsMedium}}';
final String SETTINGS_FONTS_LARGE = '{{settingsFontsLarge}}';
final String SETTINGS_AUTO_SAVE = '{{settingsAutoSave}}';
final String SETTINGS_AUTO_SAVE_DESCRIPTION = '{{settingsAutoSaveDescription}}';
final String SETTINGS_AUTO_SAVE_ON = '{{settingsAutoSaveOn}}';
final String SETTINGS_AUTO_SAVE_OFF = '{{settingsAutoSaveOff}}';
final String SETTINGS_PREVIEW = '{{settingsPreview}}';
final String SETTINGS_PREVIEW_DESCRIPTION = '{{settingsPreviewDescription}}';
final String SETTINGS_PREVIEW_ON = '{{settingsPreviewOn}}';
final String SETTINGS_PREVIEW_OFF = '{{settingsPreviewOff}}';
final String SETTINGS_THEME = '{{settingsTheme}}';
final String SETTINGS_THEME_DESCRIPTION = '{{settingsThemeDescription}}';
final String SETTINGS_THEME_LIGHT = '{{settingsThemeLight}}';
final String SETTINGS_THEME_DARK = '{{settingsThemeDark}}';


final String FOCUS_BUTTON = '{{focusButton}}';
final String HEADER_BUTTON = '{{headerButton}}';
final String ORDERED_LIST_BUTTON = '{{orderedListButton}}';
final String UNORDERED_LIST_BUTTON = '{{unorderedListButton}}';
final String BOLD_BUTTON = '{{boldButton}}';
final String ITALIC_BUTTON = '{{italicButton}}';
final String STRIKE_BUTTON = '{{strikeButton}}';

final String SHORT_CHARACTER_PHRASE = 'shortCharPhrase';
final String LONG_CHARACTER_PHRASE_SINGULAR = 'longCharPhraseSingular';
final String LONG_CHARACTER_PHRASE_PLURAL = 'longCharPhrasePlural';
final String SHORT_WORD_PHRASE = 'shortWordPhrase';
final String LONG_WORD_PHRASE_SINGULAR = 'longWordPhraseSingular';
final String LONG_WORD_PHRASE_PLURAL = 'longWordPhrasePlural';
final String READING_TIME_PHRASE = 'readingTimePhrase';

final String PREVIEW_CAPTION = '{{previewCaption}}';
final String FEEDBACK_CAPTION = '{{feedbackCaption}}';

final String DIALOG_YES = 'dialogYes';
final String DIALOG_NO = 'dialogNo';
final String DIALOG_ENTER_DOCUMENT_NAME = 'dialogEnterDocumentName';
final String DIALOG_SELECT_MARKDOWN_FILE = 'dialogSelectMarkdownFile';
final String DIALOG_DOCUMENT_NAME_INVALID = 'dialogInvalidName';
final String DIALOG_DELETE_DOCUMENT = 'dialogDeleteDocument';
final String DIALOG_SAVE = 'dialogSave';
final String DIALOG_CANCEL = 'dialogCancel';
final String DIALOG_OPEN = 'dialogOpen';
final String DIALOG_OK = 'dialogOk';
String language;

Map<String, String> german = {
    DEFAULT_DOCUMENT_NAME: 'Neu', FEEDBACK_CAPTION: 'Feedback senden',

    NEW_BUTTON_CAPTION: 'Neu', SAVE_BUTTON_CAPTION: 'Speichern', SAVE_AS_BUTTON_CAPTION:'Speichern...', COPY_BUTTON_CAPTION: 'Kopieren...', DELETE_BUTTON_CAPTION: 'Löschen...', DOCUMENTS_BUTTON_CAPTION: 'Dokumente', EXPORT_MD_BUTTON_CAPTION: 'Export als MD', EXPORT_HTML_BUTTON_CAPTION: 'Export als HTML', EXPORT_PDF_BUTTON_CAPTION: 'Export als PDF (Pre-Alpha)', IMPORT_BUTTON_CAPTION: 'Importieren...',

    SEARCH_INPUT_FIELD_PLACEHOLDER: 'Suchen...', REPLACE_INPUT_FIELD_PLACEHOLDER: 'Ersetzen...', REPLACE_CHECKBOX: 'Ersetzen', REPLACE_BUTTON: 'Ersetzen', REPLACE_ALL_BUTTON: 'Alle Ersetzen',

    SETTINGS_CAPTION: 'Einstellungen', SETTINGS_FONTS_CAPTION: 'Schrift', SETTINGS_FONTS_DESCRIPTION: 'Bestimmt die Größe der Schrift. Bei der Auswahl "Auto", wird sie automatisch an die breite des Browserfensters angepasst.',

    SETTINGS_FONTS_AUTO: 'Auto', SETTINGS_FONTS_SMALL: 'Klein', SETTINGS_FONTS_MEDIUM: 'Mittel', SETTINGS_FONTS_LARGE:'Groß',

    SETTINGS_AUTO_SAVE: 'Automatisches Speichern', SETTINGS_AUTO_SAVE_DESCRIPTION: 'Ist das automatische Speichern aktiviert, speichert OneTwoWrite deine Änderungen wenn du aufhörst zu tippen.', SETTINGS_AUTO_SAVE_ON: 'An', SETTINGS_AUTO_SAVE_OFF: 'Aus',

    SETTINGS_PREVIEW: 'Vorschau', SETTINGS_PREVIEW_DESCRIPTION: 'Zeigt die HTML-Vorchau an oder stellt sie aus.', SETTINGS_PREVIEW_ON: 'An', SETTINGS_PREVIEW_OFF: 'Aus',

    SETTINGS_THEME: 'Theme', SETTINGS_THEME_DESCRIPTION: 'Ändert das Aussehen von OneTwoWrite.', SETTINGS_THEME_LIGHT: 'Hell', SETTINGS_THEME_DARK: 'Dunkel',

    FOCUS_BUTTON: 'Fokus', HEADER_BUTTON: 'Überschrift', ORDERED_LIST_BUTTON: '1.Liste', UNORDERED_LIST_BUTTON: '\u{00B7}Liste', BOLD_BUTTON: 'F', ITALIC_BUTTON: 'K', STRIKE_BUTTON: 'S',

    SHORT_CHARACTER_PHRASE: ' B', LONG_CHARACTER_PHRASE_SINGULAR: ' Buchstabe', LONG_CHARACTER_PHRASE_PLURAL: ' Buchstaben', SHORT_WORD_PHRASE: ' W', LONG_WORD_PHRASE_SINGULAR: ' Wort', LONG_WORD_PHRASE_PLURAL: ' Wörter', READING_TIME_PHRASE: 'Lesedauer ',

    PREVIEW_CAPTION: 'Vorschau', DIALOG_YES: 'Ja', DIALOG_NO: 'Nein', DIALOG_ENTER_DOCUMENT_NAME: 'Bitte gib einen Dokumentnamen ein.', DIALOG_SAVE: 'Speichern', DIALOG_CANCEL: 'Abbrechen',

    DIALOG_OPEN: 'Auswahl', DIALOG_SELECT_MARKDOWN_FILE: 'Bitte wähle eine Datei aus.', DIALOG_DOCUMENT_NAME_INVALID: 'Der Name ist schon vergeben. Bitte wähle einen anderen.',

    DIALOG_OK: 'Ok', DIALOG_DELETE_DOCUMENT: 'Bist du sicher, dass du das Dokument \'%1\' löschen möchtest?'
};

Map<String, String> english = {
    DEFAULT_DOCUMENT_NAME: 'New', FEEDBACK_CAPTION: 'Send Feedback',

    NEW_BUTTON_CAPTION: 'New', SAVE_BUTTON_CAPTION:'Save', SAVE_AS_BUTTON_CAPTION:'Save...', COPY_BUTTON_CAPTION: 'Copy...', DELETE_BUTTON_CAPTION: 'Delete...', DOCUMENTS_BUTTON_CAPTION: 'Documents', EXPORT_MD_BUTTON_CAPTION: 'Export MD', EXPORT_HTML_BUTTON_CAPTION: 'Export HTML', EXPORT_PDF_BUTTON_CAPTION: 'Export PDF (Pre-alpha)', IMPORT_BUTTON_CAPTION: 'Import...',

    SEARCH_INPUT_FIELD_PLACEHOLDER: 'Search...', REPLACE_INPUT_FIELD_PLACEHOLDER: 'Ersetzen...', REPLACE_CHECKBOX: 'Replace', REPLACE_BUTTON: 'Replace', REPLACE_ALL_BUTTON: 'Replace All',

    SETTINGS_CAPTION: 'Settings', SETTINGS_FONTS_CAPTION: 'Fonts', SETTINGS_FONTS_DESCRIPTION: 'The font size of the editor.',

    SETTINGS_FONTS_AUTO: 'Auto', SETTINGS_FONTS_SMALL: 'Small', SETTINGS_FONTS_MEDIUM: 'Medium', SETTINGS_FONTS_LARGE:'Large',

    SETTINGS_AUTO_SAVE: 'Automatic Save', SETTINGS_AUTO_SAVE_DESCRIPTION: '', SETTINGS_AUTO_SAVE_ON: 'On', SETTINGS_AUTO_SAVE_OFF: 'Off',

    SETTINGS_PREVIEW: 'Preview', SETTINGS_PREVIEW_DESCRIPTION: '', SETTINGS_PREVIEW_ON: 'On', SETTINGS_PREVIEW_OFF: 'Off',

    SETTINGS_THEME: 'Theme', SETTINGS_THEME_DESCRIPTION: '', SETTINGS_THEME_LIGHT: 'Light', SETTINGS_THEME_DARK: 'Dark',

    FOCUS_BUTTON: 'Focus', HEADER_BUTTON: 'Header', ORDERED_LIST_BUTTON: '1.List', UNORDERED_LIST_BUTTON: '\u{00B7}List', BOLD_BUTTON: 'B', ITALIC_BUTTON: 'I', STRIKE_BUTTON: 'S',

    SHORT_CHARACTER_PHRASE: ' C', LONG_CHARACTER_PHRASE_SINGULAR: ' Character', LONG_CHARACTER_PHRASE_PLURAL: ' Characters', SHORT_WORD_PHRASE: ' W', LONG_WORD_PHRASE_SINGULAR: ' Word', LONG_WORD_PHRASE_PLURAL: ' Words', READING_TIME_PHRASE: 'Reading Time ',

    PREVIEW_CAPTION: 'Preview', DIALOG_YES: 'Yes', DIALOG_NO: 'No', DIALOG_ENTER_DOCUMENT_NAME: 'PLease enter a document name.', DIALOG_SAVE: 'Save', DIALOG_CANCEL: 'Cancel',

    DIALOG_OPEN: 'Open', DIALOG_SELECT_MARKDOWN_FILE: 'PLease select a markdown file.', DIALOG_DOCUMENT_NAME_INVALID: 'The document name is invalid. PLease try another one.',

    DIALOG_OK: 'Ok', DIALOG_DELETE_DOCUMENT: 'Are you sure want to delete document \'%1\'?'
};

String translate(String key, [List<String> parameters]) {
  String result;
  switch (language) {
    case 'de':
      result = german[key];
      break;
    default:
      result = english[key];
      break;
  }
  return Utils.stringFormat(result, parameters);
}

String getHelpHtmlFilePath() {
  switch (language) {
    case 'de':
      return 'MarkdownSyntaxGuideGerman.html';
    default:
      return 'MarkdownSyntaxGuide.html';
  }

}

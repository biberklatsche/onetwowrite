part of editor;

/**
 * Presenter für den Editor. Über diese Klasse laufen alle Änderungen, die am Editor vorgenommen werden.
 */
class EditorViewPresenter extends EditorViewListener {
  final EditorView _view;
  final EventBus _bus;
  final Model _model;
  Timer _userIsTypingTimer;
  bool _isUserTyping = false;
  Timer _calcTextStatisticsTimer;
  Timer _calcHtmlTimer;

  EditorViewPresenter(this._view, this._bus, this._model) {
    _view.setListener(this);
    _init();
  }

  void _textStatisticsCalculated(Map textStatistics) {
    _bus.fire(textStatisticsChangedEvent, textStatistics);
  }

  /**
   * Wird von der View aufgerufen, wenn sich etwas am Dokument geändert hat.
   */

  void documentEdited() {
    _model.isEdited = true;
  }

  /**
   * Wird von der View aufgerufen, wenn eine Zeile mit einer Überschrift den Focus hat.
   */

  void headerLineHasFocus(int headerType) {
    _bus.fire(headerLineHasFocusEvent, headerType);
  }

  /**
   * Wird von der View aufgerufen, wenn eine Zeile mit einer geordnete Liste den Focus hat.
   */

  void orderedListLineHasFocus() {
    _bus.fire(orderedListLineHasFocusEvent);
  }

  /**
   * Wird von der View aufgerufen, wenn eine Zeile mit einer ungeordnete Liste den Focus hat.
   */

  void unorderedListLineHasFocus() {
    _bus.fire(unorderedListLineHasFocusEvent);
  }

  /**
   * Wird von der View aufgerufen, wenn eine normale Zeile den Focus hat.
   */

  void normalLineHasFocus() {
    _bus.fire(normalLineHasFocusEvent);
  }

  /**
   * Wird von der View aufgerufen, wenn eine Zeile mit einem Zitat den Focus hat.
   */

  void blockquoteLineHasFocus() {
    _bus.fire(blockquoteLineHasFocusEvent);
  }

  /**
   * Wird von der View aufgerufen, wenn der Cursor über einem durchgestrichenem Wort steht.
   */

  void strikeRangeHasFocus() {
    _bus.fire(strikeRangeHasFocusEvent);
  }

  /**
   * Wird von der View aufgerufen, wenn der Cursor über einem fett geschriebenem Wort steht.
   */

  void boldRangeHasFocus() {
    _bus.fire(boldRangeHasFocusEvent);
  }

  /**
   * Wird von der View aufgerufen, wenn der Cursor über einem kursiv geschriebenem Wort steht.
   */

  void italicRangeHasFocus() {
    _bus.fire(italicRangeHasFocusEvent);
  }

  /**
   * Wird von der View aufgerufen, wenn der Cursor über einem normal geschriebenem Wort steht.
   */

  void normalRangeHasFocus() {
    _bus.fire(normalRangeHasFocusEvent);
  }

  bool isOnFocusMode() {
    return _model.isOnFocusMode;
  }

  void _htmlPreviewCalculated(String html) {
    _bus.fire(htmlPreviewCalculatedEvent, html);
  }

  void keyPressed() {
    if (_userIsTypingTimer != null && _userIsTypingTimer.isActive) {
      _userIsTypingTimer.cancel();
    }
    _userIsTypingTimer = new Timer(const Duration(seconds: 60), () {
      _isUserTyping = false;
      _userIsTyping(false);
    });
    if (!_isUserTyping) {
      _isUserTyping = true;
      _userIsTyping(true);
    }
    _calcTextStatistics();
    _calcHtmlPreview();
  }

  void _userIsTyping(bool isTyping) {
    if (_model.getSettings().autoSafe && !isTyping) {
      _saveDocument();
    }
    _bus.fire(userIsTypingEvent, isTyping);
  }


  void _stopUserIsTypingTimer() {
    if (_isUserTyping) {
      _isUserTyping = false;
      _userIsTyping(false);
    }
  }

  void _calcTextStatistics() {
    if (_calcTextStatisticsTimer != null && _calcTextStatisticsTimer.isActive) {
      _calcTextStatisticsTimer.cancel();
    }
    _calcTextStatisticsTimer = new Timer(const Duration(seconds: 1), () {
      ReceivePort receivePort = new ReceivePort();
      String workerUri;
      if (identical(1, 1.0)) {
        workerUri = 'src/js/dart/textStatisticsCalculator.dart.js';
      } else {
        workerUri = 'src/dart/textStatisticsCalculator.dart';
      }

      String text = _view.getContent();

      var sender = Isolate.spawnUri(Uri.parse(workerUri), [text], receivePort.sendPort);
      sender.then((_) => receivePort.first).then((textStatistics) {
        _textStatisticsCalculated(textStatistics);
      });
    });
  }

  void _calcHtmlPreview() {
    if (_model.getSettings().showPreview) {
      if (_calcHtmlTimer != null && _calcHtmlTimer.isActive) {
        _calcHtmlTimer.cancel();
      }
      _calcHtmlTimer = new Timer(const Duration(seconds: 2), () {
        ReceivePort receivePort = new ReceivePort();
        String workerUri;
        if (identical(1, 1.0)) {
          workerUri = 'src/js/dart/htmlCalculator.dart.js';
        } else {
          workerUri = 'src/dart/htmlCalculator.dart';
        }
        String text = _view.getContent();

        var sender = Isolate.spawnUri(Uri.parse(workerUri), [text], receivePort.sendPort);
        sender.then((_) => receivePort.first).then((html) {
          print(html);
          _htmlPreviewCalculated(html);
        });
      });
    }
  }

  void _init() {
    //Event vom Main, wenn die Größe des Fensters verändert wurde.
    _bus.on(windowResizeEvent).listen((_) {
      if (_model.getSettings() != null && _model.getSettings().fontSize == FontSize.AUTO) {
        _view.resize();
      }
    });

    //Event vom Main, wenn in das Fensters geklickt wurde.
    _bus.on(windowClickEvent).listen((_) {
      _view.focus();
    });

    //Event vom Main, wenn die Maus bewegt wurde
    _bus.on(mouseMoveEvent).listen((_) {
      _view.showScrollbar();
      _stopUserIsTypingTimer();
    });

    //Event vom Model, wenn der FocusMode geändert wurde.
    _bus.on(focusModeChangedEvent).listen((bool active) {
      _view.enableFocusMode(active);
      _view.focus();
    });

    //Event vom Menü, wenn der Ordered-List-Buttom gedrückt wurde.
    _bus.on(orderedListButtonClickEvent).listen((bool active) {
      if (active) {
        _view.addOrderedListLineMarker();
      } else {
        _view.removeLineMarker();
      }
      _calcHtmlPreview();
      _view.focus();
    });

    //Event vom Menü, wenn der Unordered-List-Buttom gedrückt wurde.
    _bus.on(unorderedListButtonClickEvent).listen((bool active) {
      if (active) {
        _view.addUnorderedListLineMarker();
      } else {
        _view.removeLineMarker();
      }
      _calcHtmlPreview();
      _view.focus();
    });

    //Event vom Menü, wenn der Blockquote-Buttom gedrückt wurde.
    _bus.on(blockquoteButtonClickEvent).listen((bool active) {
      if (active) {
        _view.addBlockquoteLineMarker();
      } else {
        _view.removeLineMarker();
      }
      _calcHtmlPreview();
      _view.focus();
    });

    //Event vom Menü, wenn einer der Header-Buttom gedrückt wurde.
    _bus.on(headerButtonClickEvent).listen((HeaderButtonClickEventData data) {
      if (data.active) {
        _view.addHeaderLineMarker(data.headerType);
      } else {
        _view.removeLineMarker();
      }
      _calcHtmlPreview();
      _view.focus();
    });

    //Event vom Menü, wenn der Italic-Buttom gedrückt wurde.
    _bus.on(italicButtonClickEvent).listen((bool active) {
      if (active) {
        _view.addItalicInlineMarker();
      } else {
        _view.removeItalicInlineMarker();
      }
      _calcHtmlPreview();
      _view.focus();
    });

    //Event vom Menü, wenn der Bold-Buttom gedrückt wurde.
    _bus.on(boldButtonClickEvent).listen((bool active) {
      if (active) {
        _view.addBoldInlineMarker();
      } else {
        _view.removeBoldInlineMarker();
      }
      _calcHtmlPreview();
      _view.focus();
    });

    //Event vom Menü, wenn der Strike-Buttom gedrückt wurde.
    _bus.on(strikeButtonClickEvent).listen((bool active) {
      if (active) {
        _view.addStrikeInlineMarker();
      } else {
        _view.removeStrikeInlineMarker();
      }
      _calcHtmlPreview();
      _view.focus();
    });

    //Event vom linken Menü, wenn ein Dokument geladen werden soll.
    _bus.on(loadDocumentEvent).listen((String documentName) {
      _model.getDocument(documentName).then((EditorDocument doc) {
        if (doc != null) {
          _view.setContent(doc.text);
          _calcTextStatistics();
          _calcHtmlPreview();
          _view.focus();
        }
      });
    });

    //Event vom linken Menü, wenn ein Dokument geladen werden soll.
    _bus.on(importDocumentEvent).listen((String content) {
      _view.setContent(content);
      _calcTextStatistics();
      _calcHtmlPreview();
      _view.focus();
    });

    //Event vom linken Menü, wenn ein neues Dokument erzeugt werden soll.
    _bus.on(newDocumentEvent).listen((_) {
      _model.isNewDocument = true;
      _model.isEdited = false;
      _model.currentDocumentName = I18n.translate(I18n.DEFAULT_DOCUMENT_NAME);
      _view.setContent('');
      _calcTextStatistics();
      _calcHtmlPreview();
      _view.focus();
    });

    //Event wenn durch Shortcut ein Dokument gelöscht werden soll.
    _bus.on(deleteDocumentEvent).listen((_) {
      String documentName = _model.currentDocumentName;
      Dialog.openConfirmDialog(I18n.translate(I18n.DIALOG_DELETE_DOCUMENT, [documentName]), (bool confirm) {
        if (confirm) {
          _model.deleteDocument(documentName).then((_) {
            _model.isNewDocument = true;
            _model.isEdited = false;
            _model.currentDocumentName = I18n.translate(I18n.DEFAULT_DOCUMENT_NAME);
            _view.setContent('');
            _calcTextStatistics();
            _calcHtmlPreview();
          });
          _view.focus();
        }
      });
    });

    //Event vom linken Menü, wenn ein Dokument gespeichert werden soll.
    _bus.on(copyDocumentEvent).listen((_) {
      Dialog.openSaveDialog((String documentName) {
        if (documentName != null && documentName.isNotEmpty) {
          _model.isNewDocument = false;
          _model.currentDocumentName = documentName;
          _saveDocument();
          return true;
        } else {
          return false;
        }
      });
    });

    //Event vom linken Menü, wenn ein Dokument gespeichert werden soll.
    _bus.on(saveDocumentEvent).listen((_) {
      if (!_model.isNewDocument) {
        _saveDocument();
      } else {
        Dialog.openSaveDialog((String documentName) {
          if (documentName != null && documentName.isNotEmpty) {
            _model.isNewDocument = false;
            _model.currentDocumentName = documentName;
            _saveDocument();
            return true;
          } else {
            return false;
          }
        });
      }
    });

    //Event vom linken Menü, wenn im Dokument gesucht werden soll.
    _bus.on(searchInDocumentEvent).listen((String searchString) {
      _view.clearSelection();
      if (!searchString.isEmpty) {
        _view.searchInDocument(searchString).then((bool found) {
          if (!found) {

          }
        });
      }
    });

    //Event vom linken Menü, wenn im Dokument gesucht werden soll.
    _bus.on(searchNextInDocumentEvent).listen((String searchString) {
      if (!searchString.isEmpty) {
        _view.searchNextInDocument();
      }
    });

    //Event vom linken Menü, wenn im Dokument gesucht werden soll.
    _bus.on(searchPreviousInDocumentEvent).listen((String searchString) {
      if (!searchString.isEmpty) {
        _view.searchPreviousInDocument();
      }
    });

    //Event vom linken Menü, wenn im Dokument ersetzt werden soll.
    _bus.on(replaceInDocumentEvent).listen((String replaceString) {
      _view.replaceInDocument(replaceString);
    });

    //Event vom linken Menü, wenn im Dokument ersetzt werden soll.
    _bus.on(replaceAllInDocumentEvent).listen((String replaceString) {
      _view.replaceAllInDocument(replaceString);
      _view.clearSelection();
    });

    //Event vom linken Menü, wenn im Dokument ersetzt werden soll.
    _bus.on(fontSizeChangeEvent).listen((FontSize fontSize) {
      switch (fontSize) {
        case FontSize.SMALL:
          _view.setFontSizeSmall();
          break;
        case FontSize.MEDIUM:
          _view.setFontSizeMedium();
          break;
        case FontSize.LARGE:
          _view.setFontSizeLarge();
          break;
        default:
          _view.resize();
          break;
      }
    });

    //Event vom linken Menü, wenn das Dokument heruntergeladen werden soll.
    _bus.on(downloadDocumentEvent).listen((String documentType) {
      DownloadDocumentType type = DownloadDocumentType.fromString(documentType);
      String currentDocumentName = _model.currentDocumentName;
      switch (type) {
        case DownloadDocumentType.MD:
          String content = _view.getContent();
          String name = '$currentDocumentName.md';
          DownloadUtil.download(name, content);
          break;
        case DownloadDocumentType.HTML:
          String content = _view.getContent();
          String name = '$currentDocumentName.html';
          String htmlContent = _model.convertMarkdownToHtml(currentDocumentName, content);
          DownloadUtil.download(name, htmlContent);
          break;
        case DownloadDocumentType.PDF:
          String content = _view.getContent();
          String name = '$currentDocumentName.pdf';
          String pdf = _model.convertMarkdownToPDF(currentDocumentName, content);
          DownloadUtil.download(name, pdf);
          break;
      }
    });

    //Event vom linken Menü, wenn die Preview angezeigt werden soll.
    _bus.on(showHtmlPreviewEvent).listen((bool show) {
      if (show) {
        _calcHtmlPreview();
      }
    });
    //Event von den Einstellungen wenn das Theme geändert wurde
    _bus.on(themeChangedEvent).listen((Theme theme) {
      _view.setTheme(theme);
    });
    _view.focus();
  }

  void _saveDocument() {
    String text = _view.getContent();
    String preview = _calcPreview(text);
    int chars = 0;
    int words = 0;
    int readingTimeInSeconds = 0;
    EditorDocument doc = new EditorDocument();
    doc.text = _view.getContent();
    doc.name = _model.currentDocumentName;
    doc.preview = _calcPreview(text);
    doc.chars = Utils.countChars(doc.text);
    doc.words = Utils.countWords(doc.text);
    doc.readingTimeInSeconds = Utils.calculateReadingTime(doc.words);
    _model.saveDocument(doc).then((_) {
      _view.focus();
    });
  }

  String _calcPreview(String text) {
    final int MAX_CHARS = 256;
    int endSubstring = text.length > MAX_CHARS ? MAX_CHARS : text.length;
    String firstFourLines = text.substring(0, endSubstring);
    List<String> lines = firstFourLines.split(new RegExp(r'\n'));
    String previewText = '';
    for (int line = 0; line < 4 && line < lines.length; line++) {
      previewText += lines.elementAt(line);
      previewText += '\n';
    }
    if (text.length > MAX_CHARS || lines.length > 4) {
      previewText += '...';
    } else {
      previewText = previewText.replaceAll(new RegExp(r'\n$'), '');
    }
    return previewText;
  }
}

class EditorViewImpl extends EditorView {

  JsObject jsEditor;
  Element editorElement;
  EditorViewListener listener;
  static final RegExp REGEXP_HEADER = new RegExp(r'^(#{1,6})\s');
  static final RegExp REGEXP_UNORDERED_LIST = new RegExp(r'^[*+-]+\s(?!-\s-)');
  static final RegExp REGEXP_ORDERED_LIST = new RegExp(r'^((\d+\.){1,6})\s');
  static final RegExp REGEXP_BLOCKQUOTE = new RegExp(r'^(>)+\s');
  static final RegExp REGEXP_BOLD_ITALIC = new RegExp(r'([*_]{3}(?=[^\s*_]))(.*?[*_]*)(\1)');
  static final RegExp REGEXP_BOLD = new RegExp(r'([*_]{2}(?=[^*_]))(.*?[*_]*)(\1)');
  static final RegExp REGEXP_ITALIC = new RegExp(r'([*_]{1}(?=[^*_]))(.*?[*_]*)(\1)');
  static final RegExp REGEXP_STRIKE = new RegExp(r'([-](?=\S))(.*?\S[-]*)(\1)');
  final HtmlEscape htmlEscape = new HtmlEscape();
  bool isUserTyping = false;

  EditorViewImpl() {
    _init();
  }

  void _init() {
    this.jsEditor = new JsObject(context['ace']['edit'], ['editor']);
    this.editorElement = querySelector("#editor");
    jsEditor.callMethod('getSession').callMethod('setMode', ['ace/mode/markdown']);
    jsEditor.callMethod('setHighlightActiveLine', [false]);
    jsEditor.callMethod('getSession').callMethod('setUseWrapMode', [true]);
    jsEditor.callMethod('setShowPrintMargin', [false]);
    jsEditor.callMethod('setAnimatedScroll', [true]);
    jsEditor.callMethod('getSession').callMethod('setWrapLimitRange', [64, 64]);
    jsEditor['renderer'].callMethod('setShowGutter', [false]);
    jsEditor.callMethod('setOption', ['scrollPastEnd', true]);
    editorElement.onKeyUp
      ..listen((Event event) => _documentHasChanged(event))
      ..listen((Event event) => _hideScrollbar())
      ..listen((Event event) => _proofCurrentLine())
      ..listen((Event event) => listener.keyPressed())
      ..listen((Event event) => _changeFocusedLine());
    querySelector("#editor .ace_content").onMouseUp
      ..listen((Event event) => _proofCurrentLine())
      ..listen((Event event) => _changeFocusedLine());
    _calcAndSetEditorFontSize();
  }

  void setTheme(Theme theme) {
    print(theme.editorTheme);
    jsEditor.callMethod('setTheme', [theme.editorTheme]);
  }

  void setListener(EditorViewListener listener) {
    this.listener = listener;
  }

  String getContent() {
    return jsEditor.callMethod('getValue');
  }

  void setContent(String content) {
    jsEditor.callMethod('setValue', [content, 1]);
  }

  void focus() {
    if (!jsEditor.callMethod('isFocused')) {
      jsEditor.callMethod('focus');
    }
    _changeFocusedLine();
  }

  void enableFocusMode(bool focusMode) {
    if (focusMode) {
      editorElement.classes.remove('text-normal');
      editorElement.classes.add('text-hidden');
    } else {
      editorElement.classes.remove('text-hidden');
      editorElement.classes.add('text-normal');
    }
  }

  void resize() {
    _calcAndSetEditorFontSize();
  }

  void showScrollbar() {
    ElementList scrollbars = querySelectorAll('.ace_scrollbar');
    for (Element scrollbar in scrollbars) {
      scrollbar.classes.remove('scrollbar-hidden');
      scrollbar.classes.add('scrollbar-shown');
    }
  }

  void _hideScrollbar() {
    ElementList scrollbars = querySelectorAll('.ace_scrollbar');
    for (Element scrollbar in scrollbars) {
      scrollbar.classes.remove('scrollbar-shown');
      scrollbar.classes.add('scrollbar-hidden');
    }
  }

  void _changeFocusedLine() {
    if (listener.isOnFocusMode()) {
      ElementList elements = querySelectorAll('.text-normal');
      for (Element element in elements) {
        element.classes.remove('text-normal');
      }
      Element element = _getFocusedLineGroup();
      element.classes.add('text-normal');
      centerSelection();
    }
  }

  Element _getFocusedLineGroup() {
    var aceCursor = jsEditor['selection'].callMethod('getCursor');
    int row = _getRow(aceCursor) + 1;
    DivElement rowElement = querySelector('.ace_text-layer .ace_line_group:nth-of-type($row)');
    if (rowElement != null) {
      return rowElement;
    } else {
      DivElement dummy = new DivElement();
      dummy.text = '';
      return dummy;
    }
  }

  int _getRow(var aceCursor) {
    int firstVisibleLine = jsEditor.callMethod('getFirstVisibleRow');
    int row = aceCursor['row'] - firstVisibleLine;
    return row;
  }

  void _proofCurrentLine() {
    CursorPosition cursorPosition = _getCursorPosition();
    String text = _getTextAtRow(cursorPosition.row);
    if (_isHeaderFocused(text)) {
      listener.headerLineHasFocus(getHeaderType(text));
    } else if (_isUnorderedListFocused(text)) {
      listener.unorderedListLineHasFocus();
    } else if (_isOrderedListFocused(text)) {
      listener.orderedListLineHasFocus();
    } else if (_isBlockquoteFocused(text)) {
      listener.blockquoteLineHasFocus();
    } else {
      listener.normalLineHasFocus();
    }
    listener.normalRangeHasFocus();
    if (_isStrikeFocused(text, cursorPosition.column)) {
      listener.strikeRangeHasFocus();
    }
    if (_isBoldAndItalicFocused(text, cursorPosition.column)) {
      listener.boldRangeHasFocus();
      listener.italicRangeHasFocus();
    } else if (_isBoldFocused(text, cursorPosition.column)) {
      listener.boldRangeHasFocus();
    } else if (_isItalicFocused(text, cursorPosition.column)) {
      listener.italicRangeHasFocus();
    }
  }

  String _getTextAtRow(int row) {
    return jsEditor['session'].callMethod('getLine', [row]);
  }

  bool _isHeaderFocused(String text) {
    return REGEXP_HEADER.hasMatch(text);
  }

  bool _isUnorderedListFocused(String text) {
    return REGEXP_UNORDERED_LIST.hasMatch(text);
  }

  bool _isOrderedListFocused(String text) {
    return REGEXP_ORDERED_LIST.hasMatch(text);
  }

  bool _isBlockquoteFocused(String text) {
    return REGEXP_BLOCKQUOTE.hasMatch(text);
  }

  int getHeaderType(text) {
    Match match = REGEXP_HEADER.firstMatch(text);
    if (match != null) {
      String matchedString = match[1];
      int headerType = matchedString.length;
      return headerType;
    }
    return 0;
  }

  String _removeLineMarkerFromText(String text) {
    String textWithoutLineMarker = text.replaceFirst(REGEXP_HEADER, '');
    textWithoutLineMarker = textWithoutLineMarker.replaceFirst(REGEXP_BLOCKQUOTE, '');
    textWithoutLineMarker = textWithoutLineMarker.replaceFirst(REGEXP_ORDERED_LIST, '');
    textWithoutLineMarker = textWithoutLineMarker.replaceFirst(REGEXP_UNORDERED_LIST, '');
    return textWithoutLineMarker;
  }

  void _selectCurrentLine() {
    jsEditor['selection'].callMethod('selectLine');
  }

  void clearSelection() {
    jsEditor.callMethod('exitMultiSelectMode', []);
    jsEditor.callMethod('clearSelection', []);
  }

  void centerSelection() {
    jsEditor.callMethod('centerSelection', []);
  }

  String _getSelectedText() {
    JsObject selectionRange = jsEditor['selection'].callMethod('getRange');
    String text = jsEditor['session'].callMethod('getTextRange', [selectionRange]);
    return text;
  }

  void _replaceSelectedText(String newText) {
    JsObject selectionRange = jsEditor['selection'].callMethod('getRange');
    jsEditor['session'].callMethod('replace', [selectionRange, newText]);
  }

  CursorPosition _getCursorPosition() {
    var aceCursor = jsEditor['selection'].callMethod('getCursor');
    CursorPosition cursorPosition = new CursorPosition(aceCursor['row'], aceCursor['column']);
    return cursorPosition;
  }

  void _setCursorPosition(CursorPosition cursorPosition) {
    jsEditor['selection'].callMethod('moveCursorTo', [cursorPosition.row, cursorPosition.column, true]);
  }

  void removeLineMarker() {
    CursorPosition cursorPosition = _getCursorPosition();
    _selectCurrentLine();
    String oldText = _getSelectedText();
    String textWithoutLineMarker = _removeLineMarkerFromText(oldText);
    int cursorOffset = textWithoutLineMarker.length - oldText.length;
    _replaceSelectedText(textWithoutLineMarker);
    cursorPosition.column += cursorOffset;
    _setCursorPosition(cursorPosition);
    clearSelection();
  }

  void addHeaderLineMarker(int headerType) {
    CursorPosition cursorPosition = _getCursorPosition();
    _selectCurrentLine();
    String oldText = _getSelectedText();
    String textWithoutLineMarker = _removeLineMarkerFromText(oldText);
    int cursorOffset = textWithoutLineMarker.length - oldText.length;
    switch (headerType) {
      case 1:
        _replaceSelectedText('# ' + textWithoutLineMarker);
        cursorOffset += 2;
        break;
      case 2:
        _replaceSelectedText('## ' + textWithoutLineMarker);
        cursorOffset += 3;
        break;
      case 3:
        _replaceSelectedText('### ' + textWithoutLineMarker);
        cursorOffset += 4;
        break;
      case 4:
        _replaceSelectedText('#### ' + textWithoutLineMarker);
        cursorOffset += 5;
        break;
      case 5:
        _replaceSelectedText('##### ' + textWithoutLineMarker);
        cursorOffset += 6;
        break;
      case 6:
        _replaceSelectedText('###### ' + textWithoutLineMarker);
        cursorOffset += 7;
        break;
    }
    cursorPosition.column += cursorOffset;
    _setCursorPosition(cursorPosition);
    clearSelection();
  }

  void addOrderedListLineMarker() {
    CursorPosition cursorPosition = _getCursorPosition();
    _selectCurrentLine();
    String oldText = _getSelectedText();
    String textWithoutLineMarker = _removeLineMarkerFromText(oldText);
    int cursorOffset = textWithoutLineMarker.length - oldText.length + 3;
    _replaceSelectedText('1. ' + textWithoutLineMarker);
    cursorPosition.column += cursorOffset;
    _setCursorPosition(cursorPosition);
    clearSelection();
  }

  void addUnorderedListLineMarker() {
    CursorPosition cursorPosition = _getCursorPosition();
    _selectCurrentLine();
    String oldText = _getSelectedText();
    String textWithoutLineMarker = _removeLineMarkerFromText(oldText);
    int cursorOffset = textWithoutLineMarker.length - oldText.length + 2;
    _replaceSelectedText('- ' + textWithoutLineMarker);
    cursorPosition.column += cursorOffset;
    _setCursorPosition(cursorPosition);
    clearSelection();
  }

  void addBlockquoteLineMarker() {
    CursorPosition cursorPosition = _getCursorPosition();
    _selectCurrentLine();
    String oldText = _getSelectedText();
    String textWithoutLineMarker = _removeLineMarkerFromText(oldText);
    int cursorOffset = textWithoutLineMarker.length - oldText.length + 2;
    _replaceSelectedText('> ' + textWithoutLineMarker);
    cursorPosition.column += cursorOffset;
    _setCursorPosition(cursorPosition);
    clearSelection();
  }

  void addItalicInlineMarker() {
    CursorPosition cursorPosition = _getCursorPosition();
    String selectedText = _getSelectedText();
    if (selectedText.isEmpty) {
      _selectCurrentWord();
      selectedText = _getSelectedText();
    }
    String newText = '*' + selectedText + '*';
    _replaceSelectedText(newText);
    cursorPosition.column += 1;
    _setCursorPosition(cursorPosition);
    clearSelection();
  }

  void addBoldInlineMarker() {
    CursorPosition cursorPosition = _getCursorPosition();
    String selectedText = _getSelectedText();
    if (selectedText.isEmpty) {
      _selectCurrentWord();
      selectedText = _getSelectedText();
    }
    String newText = '**' + selectedText + '**';
    _replaceSelectedText(newText);
    cursorPosition.column += 2;
    _setCursorPosition(cursorPosition);
    clearSelection();
  }

  void addStrikeInlineMarker() {
    CursorPosition cursorPosition = _getCursorPosition();
    String selectedText = _getSelectedText();
    if (selectedText.isEmpty) {
      _selectCurrentWord();
      selectedText = _getSelectedText();
    }
    String newText = '-' + selectedText + '-';
    _replaceSelectedText(newText);
    cursorPosition.column += 1;
    _setCursorPosition(cursorPosition);
    clearSelection();
  }

  void _selectCurrentWord() {
    jsEditor['selection'].callMethod('selectWord');
  }

  void removeItalicInlineMarker() {
    CursorPosition cursorPosition = _getCursorPosition();
    _selectInlineMarker(REGEXP_ITALIC, cursorPosition);
    String selectedText = _getSelectedText();
    String newText = _removeInlineMarkerInText(selectedText, REGEXP_ITALIC);
    _replaceSelectedText(newText);
    cursorPosition.column -= 1;
    _setCursorPosition(cursorPosition);
    clearSelection();
  }

  void removeBoldInlineMarker() {
    CursorPosition cursorPosition = _getCursorPosition();
    _selectInlineMarker(REGEXP_BOLD, cursorPosition);
    String selectedText = _getSelectedText();
    String newText = _removeInlineMarkerInText(selectedText, REGEXP_BOLD);
    _replaceSelectedText(newText);
    cursorPosition.column -= 2;
    _setCursorPosition(cursorPosition);
    clearSelection();
  }

  void removeStrikeInlineMarker() {
    CursorPosition cursorPosition = _getCursorPosition();
    _selectInlineMarker(REGEXP_STRIKE, cursorPosition);
    String selectedText = _getSelectedText();
    String newText = _removeInlineMarkerInText(selectedText, REGEXP_STRIKE);
    _replaceSelectedText(newText);
    cursorPosition.column -= 1;
    _setCursorPosition(cursorPosition);
    clearSelection();
  }

  String _removeInlineMarkerInText(String text, RegExp regexp) {
    Match match = regexp.firstMatch(text);
    String matchedString = '';
    if (match != null) {
      matchedString = match[2];
    }
    String textWithoutLineMarker = text.replaceFirst(regexp, matchedString);
    return textWithoutLineMarker;
  }

  bool _isStrikeFocused(text, int cursorColumn) {
    return _isInlineElementFocused(text, REGEXP_STRIKE, cursorColumn);
  }

  bool _isItalicFocused(text, int cursorColumn) {
    return _isInlineElementFocused(text, REGEXP_ITALIC, cursorColumn);
  }

  bool _isBoldFocused(text, int cursorColumn) {
    return _isInlineElementFocused(text, REGEXP_BOLD, cursorColumn);
  }

  bool _isBoldAndItalicFocused(text, int cursorColumn) {
    return _isInlineElementFocused(text, REGEXP_BOLD_ITALIC, cursorColumn);
  }

  bool _isInlineElementFocused(String text, RegExp regExp, int cursorColumn) {
    Iterable<Match> matches = regExp.allMatches(text);
    for (Match match in matches) {
      int start = match.start;
      int end = match.end;
      if (start > 0 && text[start - 1] == r'\') {
        continue;
      }
      if (cursorColumn >= start && cursorColumn <= end) {
        return true;
      }
    }
    return false;
  }

  void _selectInlineMarker(RegExp regExp, CursorPosition cursorPosition) {
    _selectCurrentLine();
    String currentText = _getSelectedText();
    clearSelection();
    Iterable<Match> matches = regExp.allMatches(currentText);
    for (Match match in matches) {
      int start = match.start;
      int end = match.end;
      if (cursorPosition.column >= start && cursorPosition.column <= end) {
        JsObject range = new JsObject(context['ace']['define']['modules']['ace/range']['Range'], [cursorPosition.row, start, cursorPosition.row, end]);
        jsEditor['selection'].callMethod('setSelectionRange', [range, false]);
        return;
      }
    }
  }

  void setFontSizeSmall() {
    _setFontSize(0.95);
  }

  void setFontSizeMedium() {
    _setFontSize(1.1);
  }

  void setFontSizeLarge() {
    _setFontSize(1.2);
  }

  void _calcAndSetEditorFontSize() {
    int width = window.outerWidth;
    double fontSizeInEm;
    if (width > 1200) {
      fontSizeInEm = 1.2;
    } else if (width > 1100) {
      fontSizeInEm = 1.1;
    } else if (width > 1000) {
      fontSizeInEm = 1.0;
    } else {
      fontSizeInEm = 0.95;
    }
    _setFontSize(fontSizeInEm);
  }

  void _setFontSize(double fontSizeInEm) {
    querySelector('#editor-container').style.width = (0.75 * fontSizeInEm * 64).toString() + 'em';
    editorElement.style.fontSize = fontSizeInEm.toString() + 'em';
  }

  void _documentHasChanged(KeyboardEvent event) {
    if (!event.ctrlKey && event.keyCode != KeyCode.ESC && event.keyCode != KeyCode.ALT && event.keyCode != KeyCode.CAPS_LOCK && event.keyCode != KeyCode.CTRL && event.keyCode != KeyCode.TAB && event.keyCode != KeyCode.DOWN && event.keyCode != KeyCode.UP && event.keyCode != KeyCode.LEFT && event.keyCode != KeyCode.RIGHT) {
      listener.documentEdited();
    }
  }

  Future<bool> searchInDocument(searchString) {
    var completer = new Completer();
    Timer.run(() {
      var mapOptions = new JsObject.jsify({
          "backwards": false, "wrap": true, "caseSensitive": false, "wholeWord": false, "skipCurrent": false, "regExp": false
      });
      var range = jsEditor.callMethod('find', [searchString, mapOptions, true]);
      completer.complete(range != null);
    });
    return completer.future;
  }

  void searchNextInDocument() {
    jsEditor.callMethod('findNext', []);
  }

  void searchPreviousInDocument() {
    jsEditor.callMethod('findPrevious', []);
  }

  void replaceInDocument(String replaceString) {
    jsEditor.callMethod('replace', [replaceString]);
  }

  void replaceAllInDocument(String replaceString) {
    jsEditor.callMethod('replaceAll', [replaceString]);
  }

}

class CursorPosition {

  CursorPosition(this.row, this.column);

  int row;
  int column;
}
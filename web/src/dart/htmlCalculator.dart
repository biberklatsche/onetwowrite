import 'dart:isolate';
import 'utils/web/webUtils.dart' as Utils;

void main(List<String> args, SendPort replyTo) {
  try {
    String text = args.first;
    String html = Utils.calcInlineHtml(text);
    replyTo.send(html);
  } catch(exception) {
    replyTo.send(exception.toString());
  }
}


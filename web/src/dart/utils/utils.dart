library web.utils;

import 'dart:math' as Math;

final int CHAR_NUMBERSIGN = 35;
final int CHAR_ASTERIX = 42;
final int CHAR_UNDERSCORE = 95;
final int CHAR_MINUS = 45;

int countChars(String text) {
  int charcount = 0;
  for (int index = 0; index < text.length;index++) {
    int codeUnit = text.codeUnitAt(index);
    if (codeUnit != CHAR_NUMBERSIGN && codeUnit != CHAR_ASTERIX && codeUnit != CHAR_UNDERSCORE && codeUnit != CHAR_MINUS) {
      charcount++;
    }
  }
  return charcount;
}

int countWords(String text) {
  int wordCount = 0;
  bool isPreviousCharANonWhitespace = false;
  for (int index = 0; index < text.length;index++) {
    int codeUnit = text.codeUnitAt(index);
    if ((codeUnit == 32 || codeUnit == 10 || codeUnit == 13) && isPreviousCharANonWhitespace) {
      wordCount++;
    }
    if (codeUnit == 32 || codeUnit == 10 || codeUnit == 13) {
      isPreviousCharANonWhitespace = false;
    } else {
      isPreviousCharANonWhitespace = true;
    }
  }
  if (isPreviousCharANonWhitespace) {
    wordCount++;
  }
  return wordCount;
}

int calculateReadingTime(int wordCount) {
  int readingTimeInSeconds = (wordCount / 3.3).ceil();
  return readingTimeInSeconds;
}

String stringFormat(String s, [List<String> parameters]) {
  if (parameters != null) {
    int i = 1;
    for (String parameter in parameters) {
      String key = '%$i';
      s = s.replaceAll(new RegExp(key), parameter);
      i++;
    }
  }
  return s;
}

String wrapWord(String word, int wordLength, int maxLength) {
  if (wordLength > maxLength) {
    maxLength = wordLength;
  }
  word = word.trim();
  int currentLength = 0;
  int length = word.length;
  List<String> words = _wrapWord(word, wordLength);
  String result = '';
  for (String currentWord in words) {
    currentLength += currentWord.length + 1;
    result += currentWord + ' ';
    if (currentLength + wordLength >= maxLength && length > maxLength) {
      break;
    }
  }
  if (length > maxLength) {
    String lastWord = words.removeLast();
    if (lastWord.length <= 3) {
      lastWord += words.removeLast();

    }
    if (lastWord.length > wordLength - 3) {
      lastWord = lastWord.substring(lastWord.length - 5, lastWord.length);
    }
    lastWord = '...' + lastWord;
    result = result + lastWord;
  }
  return result.trim();
}

List<String> _wrapWord(String word, int length) {
  List<String> parts = new List();
  word = word.trim();
  if (word.length < length) {
    parts.add(word);
  } else if (word.substring(0, length).contains('\n')) {
    parts.add(word.substring(0, word.indexOf('\n')).trim());
    parts.addAll(_wrapWord(word.substring(word.indexOf('\n') + 1), length));
  } else {
    int place = Math.min(length, Math.max(word.indexOf(new RegExp(r'\s')), word.indexOf(new RegExp(r'-'))));
    place = place < 0 ? length : place;
    parts.add(word.substring(0, place).trim());
    parts.addAll(_wrapWord(word.substring(place), length));
  }
  return parts;
}
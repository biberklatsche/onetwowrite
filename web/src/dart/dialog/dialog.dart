library dialog;

import 'dart:html';
import '../i18n/i18n.dart' as I18n;

final Element _template = querySelector('#dialog');
final Element _template_container = querySelector('#dialog-container');


void openConfirmDialog(String question, void function(bool confirm)) {
  _template_container.classes.remove('hide');
  _template_container.classes.add('show');
  _template.children.clear();
  Element questionElement = new DivElement();
  questionElement.text = question;
  Element buttonYes = new ButtonElement();
  buttonYes
    ..text = I18n.translate(I18n.DIALOG_YES)
    ..classes.add('default')
    ..onKeyDown.listen((KeyboardEvent e) {
    if (e.keyCode == KeyCode.ENTER) {
      _template_container.classes.remove('show');
      _template_container.classes.add('hide');
      function(true);
    }
    if (e.keyCode == KeyCode.ESC) {
      _template_container.classes.remove('show');
      _template_container.classes.add('hide');
      function(false);
    }
  })
    ..onClick.listen((e) {
    _template_container.classes.remove('show');
    _template_container.classes.add('hide');
    function(true);
  });
  Element buttonNo = new ButtonElement();
  buttonNo
    ..text = I18n.translate(I18n.DIALOG_NO)
    ..onClick.listen((e) {
    _template_container.classes.remove('show');
    _template_container.classes.add('hide');
    function(false);
  });
  Element buttons = new DivElement();
  buttons.id = 'buttons';
  buttons
    ..append(buttonNo)
    ..append(buttonYes);
  _template
    ..append(questionElement)
    ..append(buttons);
  buttonYes.focus();
}

void openSaveDialog(bool function(String filename)) {
  _template_container.classes.remove('hide');
  _template_container.classes.add('show');
  _template.children.clear();
  Element questionElement = new DivElement();
  questionElement.text = I18n.translate(I18n.DIALOG_ENTER_DOCUMENT_NAME);
  InputElement inputDocumentName = new InputElement();
  inputDocumentName.id = 'input-filename';
  inputDocumentName
    .onKeyUp.listen((KeyboardEvent e) {
    if (e.keyCode == KeyCode.ENTER) {
      _onSaveButtonClick(function, inputDocumentName, questionElement);
    }
    if (e.keyCode == KeyCode.ESC) {
      _onCancelButtonClick(function);
    }
  });
  Element buttonSave = new ButtonElement();
  buttonSave
    ..text = I18n.translate(I18n.DIALOG_SAVE)
    ..classes.add('default')
    ..onClick.listen((e) {
    _onSaveButtonClick(function, inputDocumentName, questionElement);
  });
  Element buttonCancel = new ButtonElement();
  buttonCancel
    ..text = I18n.translate(I18n.DIALOG_CANCEL)
    ..onClick.listen((e) {
    _onCancelButtonClick(function);
  });
  Element buttons = new DivElement();
  buttons.id = 'buttons';
  buttons
    ..append(buttonCancel)
    ..append(buttonSave);
  _template
    ..append(questionElement)
    ..append(inputDocumentName)
    ..append(buttons);
  inputDocumentName.focus();
}

void _onSaveButtonClick(bool function(String filename), InputElement inputDocumentName, Element questionElement) {
  if (function(inputDocumentName.value)) {
    _template_container.classes.remove('show');
    _template_container.classes.add('hide');
  } else {
    questionElement.text = I18n.translate(I18n.DIALOG_DOCUMENT_NAME_INVALID);
  }
}

void _onCancelButtonClick(bool function(String filename)) {
  _template_container.classes.remove('show');
  _template_container.classes.add('hide');
  function(null);
}

void openFileUploadDialog(void function(String content)) {
  _template_container.classes.remove('hide');
  _template_container.classes.add('show');
  _template.children.clear();
  Element questionElement = new DivElement();
  questionElement.text = I18n.translate(I18n.DIALOG_SELECT_MARKDOWN_FILE);
  final InputElement fileUploadInputElement = new InputElement();
  fileUploadInputElement.id = 'upload-filename';
  fileUploadInputElement.disabled = true;
  final FileUploadInputElement fileUploadElement = new FileUploadInputElement();
  fileUploadElement.onChange.listen((e) => fileUploadInputElement.value = fileUploadElement.files.isEmpty?'':fileUploadElement.files[0].name);
  fileUploadElement.accept = '.md';
  fileUploadElement.style.position = 'absolute';
  fileUploadElement.style.top = '0';
  fileUploadElement.style.right = '0';
  fileUploadElement.style.margin = '0';
  fileUploadElement.style.padding = '0';
  fileUploadElement.style.opacity = '0';
  fileUploadElement.style.width = '0';
  final ButtonElement fileUploadButton = new ButtonElement();
  fileUploadButton.text = I18n.translate(I18n.DIALOG_OPEN);
  fileUploadButton.onClick.listen((e)=>fileUploadElement.click());
  DivElement fileUpload = new DivElement();
  fileUpload.id = 'fileUploadContainer';
  fileUpload..append(fileUploadInputElement)..append(fileUploadElement)..append(fileUploadButton);


  Element buttonYes = new ButtonElement();
  buttonYes
    ..text = I18n.translate(I18n.DIALOG_OK)
    ..classes.add('default')
    ..onClick.listen((e) {
    _template_container.classes.remove('show');
    _template_container.classes.add('hide');
    final files = fileUploadElement.files;
    if (files.length == 1) {
      final file = files[0];
      final reader = new FileReader();
      reader.onLoad.listen((e) {
        String result = reader.result;
        function(result);
      });
      reader.readAsText(file);
    }

  });
  Element buttonNo = new ButtonElement();
  buttonNo
    ..text = I18n.translate(I18n.DIALOG_CANCEL)
    ..onClick.listen((e) {
    _template_container.classes.remove('show');
    _template_container.classes.add('hide');
    function(null);
  });
  Element buttons = new DivElement();
  buttons.id = 'buttons';
  buttons
    ..append(buttonNo)
    ..append(buttonYes);
  _template
    ..append(questionElement)
    ..append(fileUpload)
    ..append(buttons);
}

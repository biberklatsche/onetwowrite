part of menu.bottom;

class BottomMenuViewPresenter implements BottomMenuViewListener {
  final EventBus _bus;
  final BottomMenuView _view;
  final Model _model;

  BottomMenuViewPresenter(this._view, this._bus, this._model) {
    _view.setListener(this);
    _bus.on(headerLineHasFocusEvent).listen((int headerType) {
      _view.activateHeaderButton(headerType, true);
    });
    _bus.on(orderedListLineHasFocusEvent).listen((dynamic) {
      _view.activateOrderedListButton(true);
    });
    _bus.on(unorderedListLineHasFocusEvent).listen((dynamic) {
      _view.activateUnorderedListButton(true);
    });
    _bus.on(blockquoteLineHasFocusEvent).listen((dynamic) {
      _view.activateBlockquoteButton(true);
    });
    _bus.on(normalLineHasFocusEvent).listen((dynamic) {
      _view.deactivateAllLineFormatButtons();
    });
    _bus.on(strikeRangeHasFocusEvent).listen((dynamic) {
      _view.activateStrikeButton(true);
    });
    _bus.on(boldRangeHasFocusEvent).listen((dynamic) {
      _view.activateBoldButton(true);
    });
    _bus.on(italicRangeHasFocusEvent).listen((dynamic) {
      _view.activateItalicButton(true);
    });
    _bus.on(normalRangeHasFocusEvent).listen((dynamic) {
      _view.deactivateAllInlineFormatButtons();
    });
    _bus.on(textStatisticsChangedEvent).listen((Map textStatistics) {
      _view.setTextStatistics(textStatistics);
    });
    //Event vom Main, wenn die Größe des Fensters verändert wurde.
    _bus.on(windowResizeEvent).listen((_) {
      _view.resize();
    });
    _bus.on(userIsTypingEvent).listen((isTyping) {
      if (isTyping) {
        _view.hide();
      } else {
        _view.show();
      }
    });
  }

  void onFocusButtonClick(bool active) {
    _model.isOnFocusMode = active;
  }

  void onHeaderButtonClick(bool active, int headerType) {
    HeaderButtonClickEventData data = new HeaderButtonClickEventData();
    data.active = active;
    data.headerType = headerType;
    _bus.fire(headerButtonClickEvent, data);
  }

  void onOrderedListButtonClick(bool active) {
    _bus.fire(orderedListButtonClickEvent, active);
  }

  void onUnorderedListButtonClick(bool active) {
    _bus.fire(unorderedListButtonClickEvent, active);
  }

  void onBlockquoteButtonClick(bool active) {
    _bus.fire(blockquoteButtonClickEvent, active);
  }

  void onItalicButtonClick(bool active) {
    _bus.fire(italicButtonClickEvent, active);
  }

  void onBoldButtonClick(bool active) {
    _bus.fire(boldButtonClickEvent, active);
  }

  void onStrikeButtonClick(bool active) {
    _bus.fire(strikeButtonClickEvent, active);
  }
}

class BottomMenuViewImpl extends BottomMenuView {

  Element menu;
  BottomMenuViewListener listener;

  BottomMenuViewImpl() {
    _init();
  }

  void _init() {
    menu = querySelector("#menu-bottom");
    querySelector("#button-focus").onClick.listen((Event event) {
      _handleButtonClick(event.target);
    });
    querySelector("#button-header").onClick.listen((Event event) {
      _handleButtonClick(event.target);
    });
    querySelector("#button-bold").onClick.listen((Event event) {
      _handleButtonClick(event.target);
    });
    querySelector("#button-italic").onClick.listen((Event event) {
      _handleButtonClick(event.target);
    });
    querySelector("#button-strike").onClick.listen((Event event) {
      _handleButtonClick(event.target);
    });
    querySelector("#button-ordered-list").onClick.listen((Event event) {
      _handleButtonClick(event.target);
    });
    querySelector("#button-unordered-list").onClick.listen((Event event) {
      _handleButtonClick(event.target);
    });
    querySelector("#button-blockquote").onClick.listen((Event event) {
      _handleButtonClick(event.target);
    });
  }

  void setListener(BottomMenuViewListener listener) {
    this.listener = listener;
  }

  void setFocusButtonCaption(String caption) {
    Element focusButton = querySelector("#button-focus");
    focusButton.text = caption;
  }

  void activateHeaderButton(int headerType, bool activate) {
    Element headerButton = querySelector("#button-header");
    //Element headerButton = querySelector("#button-h$headerType");
    _activateMenuButton(headerButton, activate);
  }

  void activateOrderedListButton(bool activate) {
    Element button = querySelector("#button-ordered-list");
    _activateMenuButton(button, activate);
  }

  void activateUnorderedListButton(bool activate) {
    Element button = querySelector("#button-unordered-list");
    _activateMenuButton(button, activate);
  }

  void activateBlockquoteButton(bool activate) {
    Element button = querySelector("#button-blockquote");
    _activateMenuButton(button, activate);
  }

  void activateStrikeButton(bool activate) {
    Element button = querySelector("#button-strike");
    _activateMenuButton(button, activate);
  }

  void activateBoldButton(bool activate) {
    Element button = querySelector("#button-bold");
    _activateMenuButton(button, activate);
  }

  void activateItalicButton(bool activate) {
    Element button = querySelector("#button-italic");
    _activateMenuButton(button, activate);
  }

  void deactivateAllLineFormatButtons() {
    ElementList elements = querySelectorAll('.line-formatting');
    for (Element element in elements) {
      element.classes.remove(_BUTTON_ACTIVE);
      element.classes.add(_BUTTON_INACTIVE);
    }
  }

  void deactivateAllInlineFormatButtons() {
    ElementList elements = querySelectorAll('.inline-formatting');
    for (Element element in elements) {
      element.classes.remove(_BUTTON_ACTIVE);
      element.classes.add(_BUTTON_INACTIVE);
    }
  }

  void setTextStatistics(Map textStatistics) {
    int characters = textStatistics['characters'];
    int words = textStatistics['words'];
    int readingTimeInSeconds = textStatistics['readingTimeInSeconds'];
    int hours = (readingTimeInSeconds / 3600).floor();
    int minutes = (readingTimeInSeconds / 60).floor() % 60;
    int seconds = readingTimeInSeconds % 60;
    String hoursAsString = hours < 10 ? '0$hours' : '$hours';
    String minutesAsString = minutes < 10 ? '0$minutes' : '$minutes';
    String secondsAsString = seconds < 10 ? '0$seconds' : '$seconds';
    querySelector("#characters-value").text = '$characters';
    querySelector("#words-value").text = '$words';
    querySelector("#reading-time-value").text = '$hoursAsString:$minutesAsString:$secondsAsString';

    _setCorrectPhrases(characters == 1, words == 1);
  }

  void show() {
    if (menu.className != 'visible') {
      menu.className = 'visible';
    }
  }

  void hide() {
    if (menu.className != 'hidden') {
      menu.className = 'hidden';
    }
  }

  void _handleButtonClick(Element menuButton) {
    bool newActiveStatus = !menuButton.classes.contains(_BUTTON_ACTIVE);
    _activateMenuButton(menuButton, newActiveStatus);
    switch (menuButton.id) {
      case 'button-focus':
        listener.onFocusButtonClick(newActiveStatus);
        break;
      case 'button-header':
        listener.onHeaderButtonClick(newActiveStatus, 1);
        break;
      case 'button-h2':
        listener.onHeaderButtonClick(newActiveStatus, 2);
        break;
      case 'button-h3':
        listener.onHeaderButtonClick(newActiveStatus, 3);
        break;
      case 'button-h4':
        listener.onHeaderButtonClick(newActiveStatus, 4);
        break;
      case 'button-h5':
        listener.onHeaderButtonClick(newActiveStatus, 5);
        break;
      case 'button-h6':
        listener.onHeaderButtonClick(newActiveStatus, 6);
        break;
      case 'button-ordered-list':
        listener.onOrderedListButtonClick(newActiveStatus);
        break;
      case 'button-unordered-list':
        listener.onUnorderedListButtonClick(newActiveStatus);
        break;
      case 'button-blockquote':
        listener.onBlockquoteButtonClick(newActiveStatus);
        break;
      case 'button-italic':
        listener.onItalicButtonClick(newActiveStatus);
        break;
      case 'button-bold':
        listener.onBoldButtonClick(newActiveStatus);
        break;
      case 'button-strike':
        listener.onStrikeButtonClick(newActiveStatus);
        break;
    }
  }

  void _activateMenuButton(Element button, bool activate) {
    if (button.classes.contains('line-formatting')) {
      deactivateAllLineFormatButtons();
    }
    if (activate) {
      button.classes.remove(_BUTTON_INACTIVE);
      button.classes.add(_BUTTON_ACTIVE);
    } else {
      button.classes.remove(_BUTTON_ACTIVE);
      button.classes.add(_BUTTON_INACTIVE);
    }
  }

  void resize() {
    _setCorrectPhrases(querySelector('#characters-value').text == '1', querySelector('#words-value').text == '1');
  }

  void _setCorrectPhrases(bool charsSingular, bool wordsSingular) {
    int width = querySelector('#menu-bottom .stretcher').clientWidth;
    if (width < 10) {
      querySelector("#characters-phrase").text = I18n.translate(I18n.SHORT_CHARACTER_PHRASE);
      querySelector("#words-phrase").text = I18n.translate(I18n.SHORT_WORD_PHRASE);
      querySelector("#reading-time-phrase").text = '';
    } else if (width > 250 || querySelector("#characters-phrase").text.length > 2) {
      querySelector("#characters-phrase").text = charsSingular ? I18n.translate(I18n.LONG_CHARACTER_PHRASE_SINGULAR) : I18n.translate(I18n.LONG_CHARACTER_PHRASE_PLURAL);
      querySelector("#words-phrase").text = wordsSingular ? I18n.translate(I18n.LONG_WORD_PHRASE_SINGULAR) : I18n.translate(I18n.LONG_WORD_PHRASE_PLURAL);
      querySelector("#reading-time-phrase").text = I18n.translate(I18n.READING_TIME_PHRASE);
    }
  }
}


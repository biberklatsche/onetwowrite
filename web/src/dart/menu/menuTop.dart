library menu.top;
import 'dart:js';
import '../eventbus/eventbus.dart';
import 'dart:html';

part '../menu/menuTopImpl.dart';

abstract class TopMenuViewListener {
  void onFullscreenButtonClick();
}

abstract class TopMenuView {
  void setListener(TopMenuViewListener listener);

  void activateFullscreenMode(bool active);

  void setDocumentName(String documentName);

  void hideMenu();

  void showMenu();
}




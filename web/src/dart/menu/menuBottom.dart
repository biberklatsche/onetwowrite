library menu.bottom;
import '../eventbus/eventbus.dart';
import '../model/model.dart';
import 'dart:html';
import '../i18n/i18n.dart' as I18n;

part '../menu/menuBottomImpl.dart';

const String _BUTTON_ACTIVE = 'active';
const String _BUTTON_INACTIVE = 'inactive';

abstract class BottomMenuViewListener {
  void onFocusButtonClick(bool active);

  void onHeaderButtonClick(bool active, int headerType);

  void onOrderedListButtonClick(bool active);

  void onUnorderedListButtonClick(bool active);

  void onBlockquoteButtonClick(bool active);

  void onItalicButtonClick(bool active);

  void onBoldButtonClick(bool active);

  void onStrikeButtonClick(bool active);
}

abstract class BottomMenuView {
  void setListener(BottomMenuViewListener listener);

  void setFocusButtonCaption(String caption);

  void activateHeaderButton(int headerType, bool activate);

  void activateUnorderedListButton(bool activate);

  void activateOrderedListButton(bool activate);

  void activateBlockquoteButton(bool activate);

  void activateStrikeButton(bool activate);

  void activateBoldButton(bool activate);

  void activateItalicButton(bool activate);

  void deactivateAllLineFormatButtons();

  void deactivateAllInlineFormatButtons();

  void setTextStatistics(Map textStatistics);

  void show();

  void hide();

  void resize();
}




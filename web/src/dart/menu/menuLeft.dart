library menu.left;
import '../eventbus/eventbus.dart';
import '../dialog/dialog.dart' as Dialog;
import '../model/model.dart';
import '../i18n/i18n.dart' as I18n;
import '../utils/utils.dart' as Utils;
import 'dart:html';
import 'dart:async';
import 'dart:js';

part '../menu/menuLeftImpl.dart';

const String _BUTTON_ACTIVE = 'active';
const String _BUTTON_INACTIVE = 'inactive';

abstract class LeftMenuViewListener {
  void onDocumentsButtonClick(bool isActive);

  void onDocumentEntryClick(String documentName);

  void onDocumentEntryDoubleClick(String documentName);

  void onNewButtonClick();

  void onSaveButtonClick();

  void onCopyButtonClick();

  void onDeleteButtonClick();

  void onDetailsDeleteButtonClick(String documentName);

  void onCloseDocumentDetailsButtonClick();

  void onDocumentMenuButtonClick();

  void onSearchButtonClick(String searchText);

  void onSearchNextButtonClick(String searchText);

  void onSearchPreviousButtonClick(String searchText);

  void onReplaceCheckboxClick(bool active);

  void onReplaceButtonClick(String searchText, String replaceText);

  void onReplaceAllButtonClick(String searchText, String replaceText);

  void onExportMdButtonClick();

  void onExportPdfButtonClick();

  void onExportHtmlButtonClick();

  void onImportButtonClick();

  void onSettingsMenuButtonClick();

  void onFontSizeButtonClick(FontSize fontSize);

  void onThemeButtonClick(Theme theme);

  void onAutoSaveButtonClick(bool autoSave);

  void onShowPreviewButtonClick(bool showPreview);
}

abstract class LeftMenuView {
  void setListener(LeftMenuViewListener listener);

  void showMenu();

  void hideMenu();

  void closeMenu();

  int getDocumentsContainerWidth();

  void setDocumentsContainerWidthInPx(int width);

  int getIndexOfDocumentEntry(String documentName);

  void addDocumentEntry(String documentId, String documentName);

  void clearDocumentEntries();

  void showDocumentEntries(bool show);

  void addDocumentDetails(EditorDocument doc, int position, int arrowPositionLeftInPX, Theme theme);

  void updateDocumentDetails(EditorDocument doc, int arrowPositionLeftInPX);

  String getCurrentDetailDocumentName();

  void removeDocumentDetails();

  void enlargeIconOfDocumentEntry(String documentName);

  void reduceIconOfDocumentEntry(String documentName);

  void removeIconOfDocumentEntry(String documentName);

  void showSaveButton(bool show);

  void showSaveAsButton(bool show);

  void enableDeleteButton(bool enable);

  void enableCopyButton(bool enable);

  void showEditedWarning(bool show);

  void showReplaceField(bool show);

  void setSettingsFontSizeSmall();

  void setSettingsFontSizeMedium();

  void setSettingsFontSizeLarge();

  void setSettingsFontSizeAuto();

  void setSettingsThemeLight();

  void setSettingsThemeDark();

  void setSettingAutoSave(bool enabled);

  void setSettingShowPreview(bool enabled);
}